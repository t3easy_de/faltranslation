<?php

/** @var $extbaseObjectContainer \TYPO3\CMS\Extbase\Object\Container\Container */
$extbaseObjectContainer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class);
$extbaseObjectContainer->registerImplementation(
    \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\ColumnMap::class,
    \T3easy\Faltranslation\Persistence\Generic\Mapper\ColumnMap::class
);
$extbaseObjectContainer->registerImplementation(
    \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapFactory::class,
    \T3easy\Faltranslation\Persistence\Generic\Mapper\DataMapFactory::class
);
$extbaseObjectContainer->registerImplementation(
    \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper::class,
    \T3easy\Faltranslation\Persistence\Generic\Mapper\DataMapper::class
);
unset($extbaseObjectContainer);
